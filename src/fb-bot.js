import express from 'express';
import bodyParser from 'body-parser';
import request from 'request';
import config from '../config';

var fs = require("fs");
var http = require("http");
var https = require('https');

var privateKey  = fs.readFileSync('_private.key', 'utf8');
var certificate = fs.readFileSync('_cert.crt', 'utf8');
var ca = [
            fs.readFileSync('_ca.ca-bundle', 'utf8')
        ];
var credentials = {key: privateKey, cert: certificate, ca: ca};
console.log(credentials);


const app = express();
const port = '12200';
const portssl = '12202';
const VERIFY_TOKEN = config.VERIFY_TOKEN;
const PAGE_TOKEN = config.PAGE_TOKEN;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/webhook/', (req, res) => {
  if (req.query['hub.verify_token'] === VERIFY_TOKEN) {
    res.send(req.query['hub.challenge']);
  }
  res.send('Error, wrong validation token');
})

app.post('/webhook/', (req, res) => {

  console.log("RECV...");
  console.log(req.body);

  const messaging_events = req.body.entry[0].messaging;
  for (let i = 0; i < messaging_events.length; i++) {
    const event = req.body.entry[0].messaging[i];
    const sender = event.sender.id;
    if (event.message && event.message.text) {

      if (event.message.text === "see catalog") {
      	sendTmpl1(sender);
      }

      const text = event.message.text;
      sendTextMessage(sender, "Text received from sender: "+sender+", echo: "+ text.substring(0, 200));
      sendTextMessage(sender, "Available options: \"see catalog\"");
    } else if (event.message && event.message.attachments) {
      // sendTextMessage(sender, "Object received sender: "+sender+", echo: "+ JSON.stringify(event.message));
      if (event.message.attachments) {
      	var att = event.message.attachments[0];
      	if (att.type === "image") {
      		var image_url = att.payload.url;
      		sendTextMessage(sender, image_url);
      		sendTextMessage(sender, "What a lovely picture you sent me!");
      		sendSticker(sender);
      	}
      }
    }

	if (event.postback) {
        var text = JSON.stringify(event.postback)
        sendTextMessage(sender, "Postback received: "+text.substring(0, 200))
    }    
    console.log(event);
  }
  res.sendStatus(200);
});


var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(port);
httpsServer.listen(portssl);

function sendSticker(sender) {
  
  const messageData = {
	  "attachment": {
	      "type": "image",
	      "payload": {
	        "url": "https://fbcdn-dragon-a.akamaihd.net/hphotos-ak-xfa1/t39.1997-6/p100x100/851586_126361877548609_1351776047_n.png"
	      }
	    }
  }

  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {
        access_token:PAGE_TOKEN
    },
    method: 'POST',
    json: {
      recipient: {
        id: sender
      },
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });	
}

function sendTextMessage(sender, text) {
  
  const messageData = {
    text: text
  }

  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {
        access_token:PAGE_TOKEN
    },
    method: 'POST',
    json: {
      recipient: {
        id: sender
      },
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });
}



function sendTmpl1(sender) {
  
  const messageData = {
		"attachment": {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [{
                    "title": "IP Cam",
                    "subtitle": "Good IP Cam",
                    "image_url": "http://hothardware.com/ContentImages/NewsItem/36603/content/small_Gear-360_with-Tripod.jpg",
                    "buttons": [{
                        "type": "web_url",
                        "url": "https://www.messenger.com",
                        "title": "web url"
                    }, {
                        "type": "postback",
                        "title": "Postback",
                        "payload": "Payload for first element in a generic bubble",
                    }],
                }, {
                    "title": "360 3D Cam",
                    "subtitle": "Innovative VR Camera",
                    "image_url": "http://imagescdn.tweaktown.com/news/4/6/46467_052_360-degree-camera-vr-use-double-kickstarter-goal.jpg",
                    "buttons": [{
                        "type": "postback",
                        "title": "Postback",
                        "payload": "Payload for second element in a generic bubble",
                    }],
                }]
            }
           }
	}

  request({
    url: 'https://graph.facebook.com/v2.6/me/messages',
    qs: {
        access_token:PAGE_TOKEN
    },
    method: 'POST',
    json: {
      recipient: {
        id: sender
      },
      message: messageData,
    }
  }, function(error, response, body) {
    if (error) {
      console.log('Error sending message: ', error);
    } else if (response.body.error) {
      console.log('Error: ', response.body.error);
    }
  });	
}



